package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.mappers.StudentIdMapper;
import nure.itinf.alexandrsosnistkie.mappers.SubjectMapper;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import nure.itinf.alexandrsosnistkie.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component("subjectDAOJdbc")
public class SubjectDAOJdbc implements DAO<Subject> {

    private JdbcTemplate jdbc;

    @Autowired
    private StudentDAOJdbc studentJdbc;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    @Override
    public Subject get(long id) {
        return jdbc.queryForObject("select * from subjects where id = ?",
                new Object[]{id}, new SubjectMapper());
    }

    @Override
    public List<Subject> getAll() {
        return jdbc.query("select * from subjects", new SubjectMapper());
    }

    @Override
    public void add(Subject subject) {
        jdbc.update("insert into subjects (id, name, CEFR, id_teacher, hours) " +
                        "values (?, ?, ?, ?, ?)", subject.getId(), subject.getName(), subject.getCefr(),
                null, subject.getHours());
    }

    @Override
    public void update(Subject subject) {
        jdbc.update("update subjects set name = ?, CEFR = ?, hours = ? " +
                "where id = ?", subject.getName(), subject.getCefr(), subject.getHours(), subject.getId());
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from subjects where id = ?", id);
    }

    @Override
    public void clear() {
        jdbc.update("delete from subjects");
    }

    public void setTeacher(Subject subject, Teacher teacher) {
        jdbc.update("update subjects set id_teacher = ? where id = ?",
                teacher.getId(), subject.getId());
    }

    public List<Student> getStudents(Subject subject) {
        List<Student> students = jdbc.query("select id_student as id from student_program where id_subject = ?",
                new Object[]{subject.getId()}, new StudentIdMapper());

        List<Student> studentsResult = new ArrayList<>();

        students.forEach(s -> studentsResult.add(studentJdbc.get(s.getId())));

        return studentsResult;
    }
}
