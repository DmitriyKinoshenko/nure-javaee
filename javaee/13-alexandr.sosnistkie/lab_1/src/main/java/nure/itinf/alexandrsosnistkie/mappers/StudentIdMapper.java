package nure.itinf.alexandrsosnistkie.mappers;

import nure.itinf.alexandrsosnistkie.model.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentIdMapper implements RowMapper<Student> {
    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        return Student.builder()
                .id(resultSet.getLong("id"))
                .build();
    }
}
