package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.mappers.StudentMapper;
import nure.itinf.alexandrsosnistkie.mappers.SubjectIdMapper;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component("studentDAOJdbc")
public class StudentDAOJdbc implements DAO<Student> {

    private JdbcTemplate jdbc;

    @Autowired
    private SubjectDAOJdbc subjectJdbc;

    public void setDataSource(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    @Override
    public Student get(long id) {
        return jdbc.queryForObject("select * from students where id = ?",
                new Object[]{id}, new StudentMapper());
    }

    @Override
    public List<Student> getAll() {
        return jdbc.query("select * from students", new StudentMapper());
    }

    @Override
    public void add(Student student) {
        jdbc.update("insert into students (id, FIO, address, phone_number, email) " +
                        "values (?, ?, ?, ?, ?)", student.getId(), student.getFIO(), student.getAddress(),
                student.getPhoneNumber(), student.getEmail());
    }

    @Override
    public void update(Student student) {
        jdbc.update("update students set FIO = ?, address = ?, phone_number = ?, email = ? " +
                        "where id = ?", student.getFIO(), student.getAddress(), student.getPhoneNumber(),
                student.getEmail(), student.getId());
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from students where id = ?", id);
    }

    @Override
    public void clear() {
        jdbc.update("delete from students");
    }

    public void clearProgram() {
        jdbc.update("delete from student_program");
    }

    public List<Subject> getSubjects(Student student) {
        List<Subject> subjects = jdbc.query("select id_subject as id from student_program where id_student = ?",
                new Object[]{student.getId()}, new SubjectIdMapper());

        List<Subject> subjectsResult = new ArrayList<>();

        subjects.forEach(s -> subjectsResult.add(subjectJdbc.get(s.getId())));

        return subjectsResult;
    }



    public void setSubject(Student student, Subject subject) {
        jdbc.update("insert into student_program (id_student, id_subject) " +
                "values ( ?, ?)", student.getId(), subject.getId());
    }

    public void removeSubject(Student student, Subject subject) {
        jdbc.update("delete from student_program where id_student = ? and id_subject = ?",
                student.getId(), subject.getId());
    }

    public void removeAllSubjects(Student student) {
        jdbc.update("delete from student_program where id_student = ?", student.getId());
    }
}
