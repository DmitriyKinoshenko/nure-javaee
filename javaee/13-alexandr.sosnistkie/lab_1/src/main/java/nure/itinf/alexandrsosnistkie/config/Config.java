package nure.itinf.alexandrsosnistkie.config;

import nure.itinf.alexandrsosnistkie.dao.StudentDAOJdbc;
import nure.itinf.alexandrsosnistkie.dao.SubjectDAOJdbc;
import nure.itinf.alexandrsosnistkie.dao.TeacherDAOJdbc;
import nure.itinf.alexandrsosnistkie.dao.TimetableDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
@ComponentScan("nure.itinf.alexandrsosnistkie")
public class Config {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource mySQLDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(environment.getProperty("DataSource.url"));
        dataSource.setUsername(environment.getProperty("DataSource.username"));
        dataSource.setPassword(environment.getProperty("DataSource.password"));
        return dataSource;
    }

    @Bean
    public StudentDAOJdbc studentDAOJdbc() {
        StudentDAOJdbc studentDAOJdbc = new StudentDAOJdbc();
        studentDAOJdbc.setDataSource(mySQLDataSource());
        return studentDAOJdbc;
    }

    @Bean
    public SubjectDAOJdbc subjectDAOJdbc() {
        SubjectDAOJdbc subjectDAOJdbc = new SubjectDAOJdbc();
        subjectDAOJdbc.setDataSource(mySQLDataSource());
        return subjectDAOJdbc;
    }

    @Bean
    public TeacherDAOJdbc teacherDAOJdbc() {
        TeacherDAOJdbc teacherDAOJdbc = new TeacherDAOJdbc();
        teacherDAOJdbc.setDataSource(mySQLDataSource());
        return teacherDAOJdbc;
    }

    @Bean
    public TimetableDAO timetableDAO() {
        TimetableDAO timetableDAO = new TimetableDAO();
        timetableDAO.setDataSource(mySQLDataSource());
        return timetableDAO;
    }
}
