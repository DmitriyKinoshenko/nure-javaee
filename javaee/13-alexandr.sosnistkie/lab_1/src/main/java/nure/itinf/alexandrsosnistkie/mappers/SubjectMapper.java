package nure.itinf.alexandrsosnistkie.mappers;

import nure.itinf.alexandrsosnistkie.model.Subject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubjectMapper implements RowMapper<Subject> {
    @Override
    public Subject mapRow(ResultSet resultSet, int i) throws SQLException {
        return Subject.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .cefr(resultSet.getString("CEFR"))
                .hours(resultSet.getInt("hours"))
                .build();
    }
}
