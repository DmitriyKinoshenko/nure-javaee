package nure.itinf.alexandrsosnistkie.dao;

import java.util.List;

public interface DAO<T> {
    T get(long id);

    List<T> getAll();

    void add(T t);

    void update(T t);

    void delete(long id);

    void clear();
}
