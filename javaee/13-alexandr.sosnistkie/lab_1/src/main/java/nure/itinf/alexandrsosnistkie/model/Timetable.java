package nure.itinf.alexandrsosnistkie.model;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class Timetable {
    private Long idSubject;
    private Timestamp timeStart;
    private Timestamp timeEnd;
}
