package nure.itinf.alexandrsosnistkie.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Student {
    private Long id;
    private String FIO;
    private String address;
    private String phoneNumber;
    private String email;
    private List<Subject> subjects;
}
