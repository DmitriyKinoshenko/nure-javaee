package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.mappers.SubjectIdMapper;
import nure.itinf.alexandrsosnistkie.mappers.TeacherMapper;
import nure.itinf.alexandrsosnistkie.model.Subject;
import nure.itinf.alexandrsosnistkie.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component("teacherDAOJdbc")
public class TeacherDAOJdbc implements DAO<Teacher> {

    private JdbcTemplate jdbc;

    @Autowired
    private SubjectDAOJdbc subjectJdbc;

    public void setDataSource(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    @Override
    public Teacher get(long id) {
        Teacher teacher = jdbc.queryForObject("select * from teachers where id = ?",
                new Object[]{id}, new TeacherMapper());

        if (teacher != null) {
            if (!getSubjects(teacher).isEmpty()) {
                teacher.setSubjects(getSubjects(teacher));
            } else {
                teacher.setSubjects(null);
            }
        }
        return teacher;
    }

    @Override
    public List<Teacher> getAll() {
        List<Teacher> teachers = jdbc.query("select * from teachers", new TeacherMapper());

        for (Teacher t : teachers) {
            if (!getSubjects(t).isEmpty()) {
                t.setSubjects(getSubjects(t));
            } else {
                t.setSubjects(null);
            }
        }

        return teachers;
    }

    @Override
    public void add(Teacher teacher) {
        jdbc.update("insert into teachers (id, FIO, address, phone_number, email, study_license) " +
                        "values (?, ?, ?, ?, ?, ?)", teacher.getId(), teacher.getFIO(), teacher.getAddress(),
                teacher.getPhoneNumber(), teacher.getEmail(), teacher.getStudyLicense());
    }

    @Override
    public void update(Teacher teacher) {
        jdbc.update("update teachers set FIO = ?, address = ?, phone_number = ?, email = ?, study_license = ? " +
                        "where id = ?", teacher.getFIO(), teacher.getAddress(), teacher.getPhoneNumber(),
                teacher.getEmail(), teacher.getStudyLicense(), teacher.getId());
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from teachers where id = ?", id);
    }

    @Override
    public void clear() {
        jdbc.update("delete from teachers");
    }

    public List<Subject> getSubjects(Teacher teacher) {
        List<Subject> subjects = jdbc.query("select id from subjects where id_teacher = ?",
                new Object[]{teacher.getId()}, new SubjectIdMapper());

        List<Subject> subjectsResult = new ArrayList<>();

        subjects.forEach(s -> subjectsResult.add(subjectJdbc.get(s.getId())));

        return subjectsResult;
    }

    public void addSubject(Teacher teacher, Subject subject) {
        subjectJdbc.add(subject);
        setSubject(teacher, subject);
    }

    public void setSubject(Teacher teacher, Subject subject) {
        subjectJdbc.setTeacher(subject, teacher);
        teacher.setSubjects(getSubjects(teacher));
    }

    public void removeSubject(Subject subject) {
        jdbc.update("update subjects set id_teacher = null where id = ?", subject.getId());
    }

    public void removeAllSubjects(Teacher teacher) {
        jdbc.update("update subjects set id_teacher = null where id_teacher = ?", teacher.getId());
    }
}
