package nure.itinf.alexandrsosnistkie.mappers;

import nure.itinf.alexandrsosnistkie.model.Timetable;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TimetableMapper implements RowMapper<Timetable> {
    @Override
    public Timetable mapRow(ResultSet resultSet, int i) throws SQLException {
        return Timetable.builder()
                .idSubject(resultSet.getLong("id_subject"))
                .timeStart(resultSet.getTimestamp("time_start"))
                .timeEnd(resultSet.getTimestamp("time_end"))
                .build();
    }
}
