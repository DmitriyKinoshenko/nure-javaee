package nure.itinf.alexandrsosnistkie.mappers;

import nure.itinf.alexandrsosnistkie.model.Teacher;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TeacherMapper implements RowMapper<Teacher> {
    @Override
    public Teacher mapRow(ResultSet resultSet, int i) throws SQLException {
        return Teacher.builder()
                .id(resultSet.getLong("id"))
                .FIO(resultSet.getString("FIO"))
                .address(resultSet.getString("address"))
                .phoneNumber(resultSet.getString("phone_number"))
                .email(resultSet.getString("email"))
                .studyLicense(resultSet.getString("study_license"))
                .build();
    }
}
