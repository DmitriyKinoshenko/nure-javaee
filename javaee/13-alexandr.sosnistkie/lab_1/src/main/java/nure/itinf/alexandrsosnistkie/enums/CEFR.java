package nure.itinf.alexandrsosnistkie.enums;

public enum CEFR {
    A1("A1"),
    A2("A2"),
    B1("B1"),
    B2("B2"),
    C1("C1"),
    C2("C2");

    private String value;

    CEFR (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
