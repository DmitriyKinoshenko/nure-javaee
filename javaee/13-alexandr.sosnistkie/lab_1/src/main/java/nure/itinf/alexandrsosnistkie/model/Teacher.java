package nure.itinf.alexandrsosnistkie.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Teacher {
    private long id;
    private String FIO;
    private String address;
    private String phoneNumber;
    private String email;
    private String studyLicense;
    private List<Subject> subjects;
}
