package nure.itinf.alexandrsosnistkie.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Subject {
    private Long id;
    private String name;
    private String cefr;
    private Integer hours;
    private List<Student> students;
}
