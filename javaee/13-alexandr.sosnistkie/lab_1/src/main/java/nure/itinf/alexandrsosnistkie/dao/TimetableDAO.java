package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.mappers.TimetableMapper;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import nure.itinf.alexandrsosnistkie.model.Teacher;
import nure.itinf.alexandrsosnistkie.model.Timetable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("timetableDAO")
public class TimetableDAO {

    private JdbcTemplate jdbc;

    @Autowired
    private StudentDAOJdbc studentJdbc;

    @Autowired
    private TeacherDAOJdbc teacherJdbc;

    public void setDataSource(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    public List<Timetable> get(Subject subject) {
        return jdbc.query("select * from timetable where id_subject = ?",
                new Object[]{subject.getId()}, new TimetableMapper());
    }

    public List<Timetable> get(Student student) {
        List<Subject> subjects = studentJdbc.getSubjects(student);
        List<Timetable> timetables = new ArrayList<>();
        for (Subject s : subjects) {
            if (!get(s).isEmpty()) {
                timetables.addAll(get(s));
            }
        }
        return timetables;
    }

    public List<Timetable> get(Teacher teacher) {
        List<Subject> subjects = teacherJdbc.getSubjects(teacher);
        List<Timetable> timetables = new ArrayList<>();
        for (Subject s : subjects) {
            if (!get(s).isEmpty()) {
                timetables.addAll(get(s));
            }
        }
        return timetables;
    }

    public List<Timetable> get(Student student, Subject subject) {
        List<Timetable> timetables = get(student);

        return timetables.stream()
                .filter(t -> t.getIdSubject().equals(subject.getId()))
                .collect(Collectors.toList());
    }

    public List<Timetable> get(Teacher teacher, Subject subject) {
        List<Timetable> timetables = get(teacher);

        return timetables.stream()
                .filter(t -> t.getIdSubject().equals(subject.getId()))
                .collect(Collectors.toList());
    }

    public List<Timetable> getAll() {
        return jdbc.query("select * from timetable", new TimetableMapper());
    }


    public void add(Timetable timetable) {
        jdbc.update("insert into timetable (id_subject, time_start, time_end) " +
                "values ( ?, ?, ?)", timetable.getIdSubject(), timetable.getTimeStart(),
                timetable.getTimeEnd());
    }

    public void delete(Timetable timetable) {
        jdbc.update("delete from timetable where id_subject = ? and time_start = ?"
                , timetable.getIdSubject(), timetable.getTimeStart());
    }


    public void clear() {
        jdbc.update("delete from timetable");
    }
}
