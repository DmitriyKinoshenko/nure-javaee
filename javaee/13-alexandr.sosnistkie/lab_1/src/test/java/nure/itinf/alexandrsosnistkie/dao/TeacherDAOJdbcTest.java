package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.config.Config;
import nure.itinf.alexandrsosnistkie.enums.CEFR;
import nure.itinf.alexandrsosnistkie.model.Subject;
import nure.itinf.alexandrsosnistkie.model.Teacher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class)
public class TeacherDAOJdbcTest {

    @Autowired
    private TeacherDAOJdbc jdbc;

    @Autowired
    private SubjectDAOJdbc jdbcSubject;

    private Teacher teacher;

    @Before
    public void setUp() {
        teacher = Teacher.builder()
                .id(1L)
                .FIO("Ivanov Pert Vadimovich")
                .address("Street 123")
                .phoneNumber("096-156-9842")
                .email("pert@gmail.com")
                .studyLicense("FJD1-15SD-16D4-16D5")
                .build();
    }

    @After
    public void clear() {
        jdbc.clear();
        jdbc.removeAllSubjects(teacher);
        jdbcSubject.clear();
    }

    @Test
    public void addTeacherArrayMustBeIncreasedAndEquals() {
        jdbc.add(teacher);

        List<Teacher> teachers = jdbc.getAll();

        assertThat(teachers).hasSize(1);

        assertThat(teachers.get(0)).isEqualTo(
                Teacher.builder()
                        .id(1L)
                        .FIO("Ivanov Pert Vadimovich")
                        .address("Street 123")
                        .phoneNumber("096-156-9842")
                        .email("pert@gmail.com")
                        .studyLicense("FJD1-15SD-16D4-16D5")
                        .build()
        );
    }

    @Test
    public void updateTeacherMustChange() {
        jdbc.add(teacher);

        Teacher newTeacher = Teacher.builder()
                .id(1L)
                .FIO("Petrov Maks Nikitovich")
                .address("Street 256")
                .phoneNumber("094-157-5272")
                .email("maks@gmail.com")
                .studyLicense("T52E-15HF-17EF-15FE")
                .build();

        jdbc.update(newTeacher);

        assertThat(jdbc.get(1L)).isEqualTo(
                Teacher.builder()
                        .id(1L)
                        .FIO("Petrov Maks Nikitovich")
                        .address("Street 256")
                        .phoneNumber("094-157-5272")
                        .email("maks@gmail.com")
                        .studyLicense("T52E-15HF-17EF-15FE")
                        .build()
        );
    }

    @Test
    public void deleteTeacherMustBeDecreased() {
        Teacher anotherTeacher = Teacher.builder()
                .id(2L)
                .FIO("Petrov Maks Nikitovich")
                .address("Street 256")
                .phoneNumber("094-157-5272")
                .email("maks@gmail.com")
                .studyLicense("T52E-15HF-17EF-15FE")
                .build();

        jdbc.add(teacher);
        jdbc.add(anotherTeacher);

        List<Teacher> teachers = jdbc.getAll();

        assertThat(teachers).hasSize(2);

        jdbc.delete(2L);

        teachers = jdbc.getAll();

        assertThat(teachers).hasSize(1);
    }

    @Test
    public void addSubjectsForTeacherMustMustBeIncreasedAndEquals() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        jdbcSubject.add(subject);

        jdbc.setSubject(teacher, subject);

        List<Subject> subjects = jdbc.getSubjects(teacher);

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build()
        );
    }

    @Test
    public void deleteSubjectForTeacherMustBeDecreased() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Elementary German")
                .cefr(CEFR.A1.getValue())
                .hours(30)
                .build();

        jdbcSubject.add(subject);
        jdbcSubject.add(anotherSubject);

        jdbc.setSubject(teacher, subject);
        jdbc.setSubject(teacher, anotherSubject);

        List<Subject> subjects = jdbc.getSubjects(teacher);

        assertThat(subjects).hasSize(2);

        jdbc.removeSubject(anotherSubject);

        subjects = jdbc.getSubjects(teacher);

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(
                Subject.builder()
                        .id(1L)
                        .name("English for Specking")
                        .cefr(CEFR.A2.getValue())
                        .hours(20)
                        .build()
        );
    }

    @Test
    public void addSubjectMustBeEquals() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        jdbc.addSubject(teacher, subject);

        List<Subject> subjects = jdbc.getSubjects(teacher);

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(
                Subject.builder()
                        .id(1L)
                        .name("English for Specking")
                        .cefr(CEFR.A2.getValue())
                        .hours(20)
                        .build());
    }

    @Test
    public void getTeacherMustHaveSubjectsAndSubjectsMustBeEquals() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Elementary German")
                .cefr(CEFR.A1.getValue())
                .hours(30)
                .build();

        jdbc.add(teacher);

        jdbc.addSubject(teacher, subject);
        jdbc.addSubject(teacher, anotherSubject);

        List<Subject> subjects = teacher.getSubjects();

        assertThat(subjects).hasSize(2);

        assertThat(subjects.get(0)).isEqualTo(
                Subject.builder()
                        .id(1L)
                        .name("English for Specking")
                        .cefr(CEFR.A2.getValue())
                        .hours(20)
                        .build()
        );

        assertThat(subjects.get(1)).isEqualTo(
                Subject.builder()
                        .id(2L)
                        .name("Elementary German")
                        .cefr(CEFR.A1.getValue())
                        .hours(30)
                        .build()
        );
    }
}