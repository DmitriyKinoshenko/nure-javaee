package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.config.Config;
import nure.itinf.alexandrsosnistkie.enums.CEFR;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class)
public class StudentDAOJdbcTest {

    @Autowired
    private StudentDAOJdbc jdbc;

    @Autowired
    private SubjectDAOJdbc jdbcSubject;

    private Student student;

    @Before
    public void setUp() {
        student = Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Zoloto 123")
                .phoneNumber("096-156-2525")
                .email("alex@gmail.com")
                .build();
    }

    @After
    public void clear() {
        jdbc.clearProgram();
        jdbc.clear();
        jdbcSubject.clear();
    }

    @Test
    public void addStudentArrayMustBeIncreasedAndEquals() {
        jdbc.add(student);

        List<Student> students = jdbc.getAll();

        assertThat(students).hasSize(1);

        assertThat(students.get(0)).isEqualTo(
                Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Zoloto 123")
                .phoneNumber("096-156-2525")
                .email("alex@gmail.com")
                .build()
        );
    }

    @Test
    public void updateStudentMustChange() {
        jdbc.add(student);

        Student newStudent = Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Saltovka 16")
                .phoneNumber("096-156-2525")
                .email("nealex@gmail.com")
                .build();

        jdbc.update(newStudent);

        assertThat(jdbc.get(1L)).isEqualTo(
                Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Saltovka 16")
                .phoneNumber("096-156-2525")
                .email("nealex@gmail.com")
                .build()
        );
    }

    @Test
    public void deleteStudentMustBeDecreased() {
        Student anotherStudent = Student.builder()
                .id(2L)
                .FIO("Petrov Oleg Vladimirovich")
                .address("Street 26")
                .phoneNumber("095-376-1618")
                .email("oleg@gmail.com")
                .build();

        jdbc.add(student);
        jdbc.add(anotherStudent);

        List<Student> students = jdbc.getAll();

        assertThat(students).hasSize(2);

        jdbc.delete(2L);

        students = jdbc.getAll();

        assertThat(students).hasSize(1);
    }

    @Test
    public void addSubjectsForStudentMustMustBeIncreasedAndEquals() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        jdbcSubject.add(subject);

        jdbc.setSubject(student, subject);

        List<Subject> subjects = jdbc.getSubjects(student);

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build()
        );
    }

    @Test
    public void deleteSubjectForStudentMustBeDecreased() {
        Subject subject = Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build();

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Elementary German")
                .cefr(CEFR.A1.getValue())
                .hours(30)
                .build();

        jdbcSubject.add(subject);
        jdbcSubject.add(anotherSubject);

        jdbc.setSubject(student, subject);
        jdbc.setSubject(student, anotherSubject);

        List<Subject> subjects = jdbc.getSubjects(student);

        assertThat(subjects).hasSize(2);

        jdbc.removeSubject(student, anotherSubject);

        subjects = jdbc.getSubjects(student);

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(
                Subject.builder()
                .id(1L)
                .name("English for Specking")
                .cefr(CEFR.A2.getValue())
                .hours(20)
                .build()
        );

        jdbc.setSubject(student, anotherSubject);

        jdbc.removeAllSubjects(student);

        subjects = jdbc.getSubjects(student);

        assertThat(subjects).hasSize(0);
    }
}