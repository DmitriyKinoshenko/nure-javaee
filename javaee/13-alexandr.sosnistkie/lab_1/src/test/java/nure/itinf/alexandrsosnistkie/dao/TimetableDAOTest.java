package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.config.Config;
import nure.itinf.alexandrsosnistkie.enums.CEFR;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import nure.itinf.alexandrsosnistkie.model.Teacher;
import nure.itinf.alexandrsosnistkie.model.Timetable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Timestamp;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class)
public class TimetableDAOTest {

    @Autowired
    private TimetableDAO jdbc;

    @Autowired
    private SubjectDAOJdbc jdbcSubject;

    @Autowired
    private StudentDAOJdbc jdbcStudent;

    @Autowired
    private TeacherDAOJdbc jdbcTeacher;

    private Timetable timetable;

    private Subject subject;

    @Before
    public void setUp() {
        subject = Subject.builder()
                .id(1L)
                .name("English for Tech")
                .cefr(CEFR.B2.getValue())
                .hours(60)
                .build();

        jdbcSubject.add(subject);

        timetable = Timetable.builder()
                .idSubject(subject.getId())
                .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                .build();
    }

    @After
    public void clear() {
        jdbc.clear();
        jdbcSubject.clear();
        jdbcStudent.clear();
        jdbcStudent.clearProgram();
        jdbcTeacher.clear();
    }

    @Test
    public void addStudentArrayMustBeIncreasedAndEquals() {
        jdbc.add(timetable);

        List<Timetable> timetables = jdbc.get(subject);

        assertThat(timetables).hasSize(1);

        assertThat(timetables.get(0)).isEqualTo(
                Timetable.builder()
                        .idSubject(subject.getId())
                        .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                        .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                        .build()
        );
    }

    @Test
    public void deleteStudentMustBeDecreased() {

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Advanced English")
                .cefr(CEFR.C1.getValue())
                .hours(40)
                .build();

        Timetable anotherTimetable = Timetable.builder()
                .idSubject(anotherSubject.getId())
                .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                .build();

        jdbcSubject.add(anotherSubject);
        jdbc.add(timetable);
        jdbc.add(anotherTimetable);

        List<Timetable> timetables = jdbc.getAll();

        assertThat(timetables).hasSize(2);

        jdbc.delete(anotherTimetable);

        timetables = jdbc.getAll();

        assertThat(timetables).hasSize(1);
    }

    @Test
    public void getByStudentAndSubject() {
        Student student = Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Zoloto 123")
                .phoneNumber("096-156-2525")
                .email("alex@gmail.com")
                .build();

        jdbcStudent.add(student);

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Advanced English")
                .cefr(CEFR.C1.getValue())
                .hours(40)
                .build();

        jdbcSubject.add(anotherSubject);

        Timetable anotherTimetable = Timetable.builder()
                .idSubject(anotherSubject.getId())
                .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                .build();

        jdbc.add(timetable);
        jdbc.add(anotherTimetable);

        jdbcStudent.setSubject(student, subject);
        jdbcStudent.setSubject(student, anotherSubject);

        List<Timetable> timetables = jdbc.get(student);

        assertThat(timetables).hasSize(2);

        assertThat(timetables.get(0)).isEqualTo(
                Timetable.builder()
                        .idSubject(subject.getId())
                        .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                        .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                        .build()
        );

        timetables = jdbc.get(student, anotherSubject);

        assertThat(timetables).hasSize(1);

        assertThat(timetables.get(0)).isEqualTo(
                Timetable.builder()
                        .idSubject(anotherSubject.getId())
                        .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                        .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                        .build()
        );
    }

    @Test
    public void getByTeacherAndSubject() {
        Teacher teacher = Teacher.builder()
                .id(1L)
                .FIO("Ivanov Pert Vadimovich")
                .address("Street 123")
                .phoneNumber("096-156-9842")
                .email("pert@gmail.com")
                .studyLicense("FJD1-15SD-16D4-16D5")
                .build();

        jdbcTeacher.add(teacher);

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Advanced English")
                .cefr(CEFR.C1.getValue())
                .hours(40)
                .build();

        jdbcSubject.add(anotherSubject);

        Timetable anotherTimetable = Timetable.builder()
                .idSubject(anotherSubject.getId())
                .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                .build();

        jdbc.add(timetable);
        jdbc.add(anotherTimetable);

        jdbcTeacher.setSubject(teacher, subject);
        jdbcTeacher.setSubject(teacher, anotherSubject);

        List<Timetable> timetables = jdbc.get(teacher);

        assertThat(timetables).hasSize(2);

        assertThat(timetables.get(0)).isEqualTo(
                Timetable.builder()
                        .idSubject(subject.getId())
                        .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                        .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                        .build()
        );

        timetables = jdbc.get(teacher, anotherSubject);

        assertThat(timetables).hasSize(1);

        assertThat(timetables.get(0)).isEqualTo(
                Timetable.builder()
                        .idSubject(anotherSubject.getId())
                        .timeStart(new Timestamp(121, 4, 30, 14, 30, 0, 0))
                        .timeEnd(new Timestamp(121, 4, 30, 16, 0, 0, 0))
                        .build()
        );
    }
}