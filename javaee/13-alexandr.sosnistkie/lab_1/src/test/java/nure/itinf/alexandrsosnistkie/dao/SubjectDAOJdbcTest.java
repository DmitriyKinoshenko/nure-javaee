package nure.itinf.alexandrsosnistkie.dao;

import nure.itinf.alexandrsosnistkie.config.Config;
import nure.itinf.alexandrsosnistkie.enums.CEFR;
import nure.itinf.alexandrsosnistkie.model.Student;
import nure.itinf.alexandrsosnistkie.model.Subject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Config.class, loader = AnnotationConfigContextLoader.class)
public class SubjectDAOJdbcTest {

    @Autowired
    private SubjectDAOJdbc jdbc;

    @Autowired
    private StudentDAOJdbc jdbcStudent;

    private Subject subject;

    @Before
    public void setUp() {
        subject = Subject.builder()
                .id(1L)
                .name("English for Tech")
                .cefr(CEFR.B2.getValue())
                .hours(60)
                .build();
    }

    @After
    public void clear() {
        jdbcStudent.clearProgram();
        jdbc.clear();
        jdbcStudent.clear();
    }

    @Test
    public void addSubjectArrayMustBeIncreasedAndEquals() {
        jdbc.add(subject);

        List<Subject> subjects = jdbc.getAll();

        assertThat(subjects).hasSize(1);

        assertThat(subjects.get(0)).isEqualTo(
                Subject.builder()
                        .id(1L)
                        .name("English for Tech")
                        .cefr(CEFR.B2.getValue())
                        .hours(60)
                        .build()
        );
    }

    @Test
    public void updateSubjectMustChange() {

        jdbc.add(subject);

        Subject newSubject = Subject.builder()
                .id(1L)
                .name("Spanish for beginners")
                .cefr(CEFR.A1.getValue())
                .hours(46)
                .build();

        jdbc.update(newSubject);

        assertThat(jdbc.get(1L)).isEqualTo(
                Subject.builder()
                        .id(1L)
                        .name("Spanish for beginners")
                        .cefr(CEFR.A1.getValue())
                        .hours(46)
                        .build()
        );
    }

    @Test
    public void deleteSubjectMustBeDecreased() {

        Subject anotherSubject = Subject.builder()
                .id(2L)
                .name("Advanced English")
                .cefr(CEFR.C1.getValue())
                .hours(40)
                .build();

        jdbc.add(subject);
        jdbc.add(anotherSubject);

        List<Subject> subjects = jdbc.getAll();

        assertThat(subjects).hasSize(2);

        jdbc.delete(2L);

        subjects = jdbc.getAll();

        assertThat(subjects).hasSize(1);
    }

    @Test
    public void getStudentsMustBeEquals() {
        Student student = Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Zoloto 123")
                .phoneNumber("096-156-2525")
                .email("alex@gmail.com")
                .build();

        jdbcStudent.add(student);

        Student anotherStudent = Student.builder()
                .id(2L)
                .FIO("Petrov Oleg Vladimirovich")
                .address("Street 26")
                .phoneNumber("095-376-1618")
                .email("oleg@gmail.com")
                .build();

        jdbcStudent.add(anotherStudent);

        jdbc.add(subject);

        jdbcStudent.setSubject(student, subject);
        jdbcStudent.setSubject(anotherStudent, subject);

        List<Student> students = jdbc.getStudents(subject);

        assertThat(students).hasSize(2);

        assertThat(students.get(0)).isEqualTo(
                Student.builder()
                .id(1L)
                .FIO("Sosnistkie Alexandr Vadimovich")
                .address("Zoloto 123")
                .phoneNumber("096-156-2525")
                .email("alex@gmail.com")
                .build()
        );

        assertThat(students.get(1)).isEqualTo(
                Student.builder()
                .id(2L)
                .FIO("Petrov Oleg Vladimirovich")
                .address("Street 26")
                .phoneNumber("095-376-1618")
                .email("oleg@gmail.com")
                .build()
        );
    }
}