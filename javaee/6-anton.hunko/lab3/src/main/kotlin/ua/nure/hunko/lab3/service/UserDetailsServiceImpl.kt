package ua.nure.hunko.lab3.service

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import ua.nure.hunko.lab3.repository.UserRepository

@Service
class UserDetailsServiceImpl(
	private val userRepository: UserRepository
) : UserDetailsService {
	override fun loadUserByUsername(username: String?): UserDetails {
		val user = userRepository.findByUsername(
			username ?: throw IllegalArgumentException("Username cannot be empty")
		)

		println("user: $user")
		return User(
			user?.username,
			user?.password,
			user?.authorities?.map(::SimpleGrantedAuthority)
		)
	}
}
