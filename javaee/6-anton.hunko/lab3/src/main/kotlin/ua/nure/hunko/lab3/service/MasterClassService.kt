package ua.nure.hunko.lab3.service

import ua.nure.hunko.lab3.model.MasterClass

interface MasterClassService {

	fun findAllMasterClasses(): List<MasterClass>

	fun findById(masterClassId: String): MasterClass

	fun addMasterClass(masterClass: MasterClass): MasterClass

	fun deleteMasterClass(masterClassId: String)
}
