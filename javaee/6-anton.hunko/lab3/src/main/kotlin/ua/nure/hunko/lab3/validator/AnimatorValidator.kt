package ua.nure.hunko.lab3.validator

import org.springframework.stereotype.Component
import ua.nure.hunko.lab3.exception.InvalidAnimatorException
import ua.nure.hunko.lab3.model.Animator

@Component
class AnimatorValidator : Validator<Animator> {

	override fun validate(t: Animator) {
		if (!ValidatorUtil.isValidEmail(t.email)) {
			throw InvalidAnimatorException("Invalid email: ${t.email}")
		}
		if (!ValidatorUtil.isValidPhone(t.phone)) {
			throw InvalidAnimatorException("Invalid phone: ${t.phone}")
		}
	}
}
