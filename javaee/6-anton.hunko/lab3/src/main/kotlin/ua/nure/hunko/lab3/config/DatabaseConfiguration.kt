package ua.nure.hunko.lab3.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.MongoDatabaseFactory
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.mapping.MongoMappingContext

@Configuration
class DatabaseConfiguration(
	private val mongoMappingContext: MongoMappingContext,
	private val mongoDatabaseFactory: MongoDatabaseFactory
) {

	@Bean
	fun databaseReferenceResolver(): DefaultDbRefResolver {
		return DefaultDbRefResolver(mongoDatabaseFactory)
	}

	@Bean
	fun mappingMongoConverter(): MappingMongoConverter {
		val converter = MappingMongoConverter(databaseReferenceResolver(), mongoMappingContext)
		converter.setTypeMapper(DefaultMongoTypeMapper(null))
		return converter
	}
}
