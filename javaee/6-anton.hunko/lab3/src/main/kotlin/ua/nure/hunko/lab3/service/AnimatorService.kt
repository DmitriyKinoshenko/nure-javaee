package ua.nure.hunko.lab3.service

import ua.nure.hunko.lab3.model.Animator

interface AnimatorService {

	fun findAllAnimators(): List<Animator>

	fun findById(animatorId: String) : Animator

	fun addAnimator(animator: Animator): Animator

	fun updateAnimator(animator: Animator, animatorId: String)
}
