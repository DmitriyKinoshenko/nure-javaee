package ua.nure.hunko.lab3.service

import org.springframework.stereotype.Service
import ua.nure.hunko.lab3.exception.ResourceNotFoundException
import ua.nure.hunko.lab3.model.MasterClass
import ua.nure.hunko.lab3.repository.MasterClassRepository
import ua.nure.hunko.lab3.validator.Validator

@Service
class MasterClassServiceImpl(
	private val masterClassRepository: MasterClassRepository,
	private val masterClassValidator: Validator<MasterClass>
) : MasterClassService {

	override fun findAllMasterClasses() = masterClassRepository.findAll().toList()

	override fun findById(masterClassId: String): MasterClass {
		return masterClassRepository.findById(masterClassId)
			.orElseThrow { ResourceNotFoundException("There is no master-class with id: $masterClassId") }
	}

	override fun addMasterClass(masterClass: MasterClass): MasterClass {
		masterClassValidator.validate(masterClass)

		return masterClassRepository.save(masterClass)
	}

	override fun deleteMasterClass(masterClassId: String) {
		if (!masterClassRepository.existsById(masterClassId)) {
			throw ResourceNotFoundException("There is no master-class with id: $masterClassId")
		}
		masterClassRepository.deleteById(masterClassId)
	}
}
