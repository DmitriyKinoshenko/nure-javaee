package ua.nure.hunko.lab3.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.util.*

@Document(collection = "master_classes")
data class MasterClass(
	@Id
	var id: String?,

	@Field("name")
	var name: String?,

	@Field("direction")
	var direction: String?,

	@Field("number_of_participants")
	var numberOfParticipants: Int?,

	@Field("start_date")
	var startDate: Date?,

	@Field("finish_date")
	var finishDate: Date?,

	@Field("animators")
	var animators: List<String?>
)
