package ua.nure.hunko.lab3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@SpringBootApplication
@EnableMongoRepositories
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class Lab3Application

fun main(args: Array<String>) {
	runApplication<Lab3Application>(*args)
}
