package ua.nure.hunko.lab3.repository

import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import ua.nure.hunko.lab3.model.User

@Repository
interface UserRepository : MongoRepository<User, ObjectId>{
	fun findByUsername(username: String) : User?
}
