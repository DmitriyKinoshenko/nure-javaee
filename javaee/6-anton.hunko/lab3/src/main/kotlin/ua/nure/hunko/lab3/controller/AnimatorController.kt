package ua.nure.hunko.lab3.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import ua.nure.hunko.lab3.model.Animator
import ua.nure.hunko.lab3.service.AnimatorService

@RestController
@RequestMapping("animators")
class AnimatorController(
	private val animatorService: AnimatorService
) {

	@GetMapping
	@PreAuthorize("permitAll()")
	fun findAllAnimators() = animatorService.findAllAnimators()

	@GetMapping("/{animatorId}")
	@PreAuthorize("hasAnyAuthority('user', 'admin')")
	fun findAnimatorById(
		@PathVariable("animatorId") animatorId: String
	) = animatorService.findById(animatorId)

	@PostMapping()
	@PreAuthorize("hasAuthority('admin')")
	fun addAnimator(
		@RequestBody animator: Animator
	) = ResponseEntity(animatorService.addAnimator(animator), HttpStatus.CREATED)

	@PutMapping("/{animatorId}")
	@PreAuthorize("hasAuthority('admin')")
	fun updateAnimator(
		@RequestBody animator: Animator,
		@PathVariable("animatorId") animatorId: String
	): ResponseEntity<HttpStatus> {
		animatorService.updateAnimator(animator, animatorId)
		return ResponseEntity(HttpStatus.NO_CONTENT)
	}
}
