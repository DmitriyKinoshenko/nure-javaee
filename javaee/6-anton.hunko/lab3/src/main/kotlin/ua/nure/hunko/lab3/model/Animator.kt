package ua.nure.hunko.lab3.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "animators")
data class Animator(
	@Id
	var id: String?,

	@Field("first_name")
	var firstName: String?,

	@Field("last_name")
	var lastName: String?,

	@Field("age")
	var age: Int?,

	@Field("phone")
	var phone: String?,

	@Field("email")
	var email: String?
)
