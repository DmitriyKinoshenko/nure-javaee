package ua.nure.hunko.lab3.advice

import com.mongodb.MongoWriteException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import ua.nure.hunko.lab3.exception.InvalidAnimatorException
import ua.nure.hunko.lab3.exception.InvalidMasterClassException

@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

	@ExceptionHandler(InvalidAnimatorException::class, InvalidMasterClassException::class)
	fun handleValidationFailedExceptions(e: RuntimeException, request: WebRequest): ResponseEntity<Any> {
		return handleExceptionInternal(e, e.message, HttpHeaders.EMPTY, HttpStatus.UNPROCESSABLE_ENTITY, request)
	}

	@ExceptionHandler(MongoWriteException::class)
	fun handleDocumentFailedValidationException(e: RuntimeException, request: WebRequest): ResponseEntity<Any> {
		return handleExceptionInternal(e, "Validation failed", HttpHeaders.EMPTY, HttpStatus.UNPROCESSABLE_ENTITY, request)
	}
}
