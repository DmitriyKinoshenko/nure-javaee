package ua.nure.hunko.lab3.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import ua.nure.hunko.lab3.service.UserDetailsServiceImpl

@Configuration
class SecurityConfiguration(
	private val userDetailsService: UserDetailsServiceImpl
) : WebSecurityConfigurerAdapter() {

	override fun configure(http: HttpSecurity) {
		http
			.httpBasic()
			.and().sessionManagement().disable()
			.csrf().disable()
	}

	@Bean
	fun passwordEncoder(): BCryptPasswordEncoder {
		return BCryptPasswordEncoder()
	}

	override fun configure(builder: AuthenticationManagerBuilder) {
		builder.userDetailsService(userDetailsService)
	}
}
