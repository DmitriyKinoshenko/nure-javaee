package ua.nure.hunko.lab3.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import ua.nure.hunko.lab3.model.Animator

@Repository
interface AnimatorRepository : MongoRepository<Animator, String>
