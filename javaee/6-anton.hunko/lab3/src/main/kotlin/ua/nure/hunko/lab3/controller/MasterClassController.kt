package ua.nure.hunko.lab3.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import ua.nure.hunko.lab3.model.MasterClass
import ua.nure.hunko.lab3.service.MasterClassService

@RestController
@RequestMapping("master-classes")
class MasterClassController(
	private val masterClassService: MasterClassService
) {
	@GetMapping
	@PreAuthorize("permitAll()")
	fun findAllMasterClasses() = masterClassService.findAllMasterClasses()

	@GetMapping("/{masterClassId}")
	@PreAuthorize("permitAll()")
	fun findMasterClassById(
		@PathVariable("masterClassId") masterClassId: String
	) = masterClassService.findById(masterClassId)

	@PostMapping
	@PreAuthorize("hasAuthority('admin')")
	fun addMasterClass(
		@RequestBody masterClass: MasterClass
	) = ResponseEntity(masterClassService.addMasterClass(masterClass), HttpStatus.CREATED)

	@DeleteMapping("/{masterClassId}")
	@PreAuthorize("hasAuthority('admin')")
	fun deleteMasterClass(
		@PathVariable masterClassId: String
	): ResponseEntity<HttpStatus> {
		masterClassService.deleteMasterClass(masterClassId)
		return ResponseEntity(HttpStatus.ACCEPTED)
	}
}
