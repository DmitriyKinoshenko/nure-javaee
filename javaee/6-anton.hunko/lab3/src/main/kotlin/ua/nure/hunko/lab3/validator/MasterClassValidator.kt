package ua.nure.hunko.lab3.validator

import org.springframework.stereotype.Component
import ua.nure.hunko.lab3.exception.InvalidMasterClassException
import ua.nure.hunko.lab3.model.Direction
import ua.nure.hunko.lab3.model.MasterClass

@Component
class MasterClassValidator : Validator<MasterClass> {

	override fun validate(t: MasterClass) {
		if (!isValidDirection(t)) {
			throw InvalidMasterClassException("Invalid direction: ${t.direction}")
		}

		if (!isValidDates(t)) {
			throw InvalidMasterClassException("Invalid date of master-class: ${t.startDate} - ${t.finishDate}")
		}
	}

	private fun isValidDirection(masterClass: MasterClass): Boolean {
		return Direction.values().any { it.title == masterClass.direction }
	}

	private fun isValidDates(masterClass: MasterClass): Boolean {
		return masterClass.startDate?.before(masterClass.finishDate) ?: true
	}

}
