package ua.nure.hunko.lab3.validator

interface Validator<T> {
	fun validate(t: T)
}
