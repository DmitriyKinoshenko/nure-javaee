package ua.nure.hunko.lab3.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "users")
data class User(
	@Id
	var id: String?,

	@Field("username")
	var username: String?,

	@Field("password")
	var password: String?,

	@Field("authorities")
	var authorities: List<String?>
)
