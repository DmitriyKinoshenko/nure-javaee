package ua.nure.hunko.lab3.model

enum class Direction(val title: String) {
	ARCHAEOLOGIST("Archaeologist"),
	POLICEMAN("Policeman"),
	FIREFIGHTER("Firefighter"),
	VETERINARIAN("Veterinarian")
}
