package ua.nure.hunko.lab3.exception

class InvalidMasterClassException : RuntimeException {
	constructor(message: String) : super(message)
	constructor(message: String, cause: Throwable) : super(message, cause)
}
