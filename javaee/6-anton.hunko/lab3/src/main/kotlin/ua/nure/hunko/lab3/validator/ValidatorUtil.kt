package ua.nure.hunko.lab3.validator

object ValidatorUtil {

	private val emailRegex = Regex("^\\w+@\\w+\\.\\w+$")
	private val phoneRegex = Regex("^\\w+\$")

	fun isValidEmail(email: String?) = email?.matches(emailRegex) ?: true

	fun isValidPhone(phone: String?) = phone?.matches(phoneRegex) ?: true
}
