package ua.nure.hunko.lab3.service

import org.springframework.beans.BeanUtils
import org.springframework.stereotype.Service
import ua.nure.hunko.lab3.exception.ResourceNotFoundException
import ua.nure.hunko.lab3.model.Animator
import ua.nure.hunko.lab3.repository.AnimatorRepository
import ua.nure.hunko.lab3.validator.Validator

@Service
class AnimatorServiceImpl(
	private val animatorRepository: AnimatorRepository,
	private val animatorValidator: Validator<Animator>
) : AnimatorService {

	override fun findAllAnimators() = animatorRepository.findAll().toList()

	override fun findById(animatorId: String): Animator {
		return animatorRepository.findById(animatorId)
			.orElseThrow { ResourceNotFoundException("There is no animator with id: $animatorId") }
	}

	override fun addAnimator(animator: Animator): Animator {
		animatorValidator.validate(animator)

		return animatorRepository.save(animator)
	}

	override fun updateAnimator(animator: Animator, animatorId: String) {
		val persistedAnimator = findById(animatorId)

		BeanUtils.copyProperties(animator, persistedAnimator, "id")

		addAnimator(persistedAnimator)
	}
}
