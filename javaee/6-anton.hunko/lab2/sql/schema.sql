DROP DATABASE IF EXISTS entertainment_center;
CREATE DATABASE entertainment_center;
USE entertainment_center;

DROP TABLE IF EXISTS master_class_booking;
DROP TABLE IF EXISTS master_class;
DROP TABLE IF EXISTS animator;
DROP TABLE IF EXISTS direction;

CREATE TABLE direction
(
    id   INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    PRIMARY KEY (id)
);

INSERT INTO direction(name)
VALUES ('Veterinarian');

INSERT INTO direction(name)
VALUES ('Archaeologist');

INSERT INTO direction(name)
VALUES ('Policeman');

INSERT INTO direction(name)
VALUES ('Firefighter');

CREATE TABLE animator
(
    id         INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255),
    last_name  VARCHAR(255),
    age        INT,
    PRIMARY KEY (id)
);

INSERT INTO animator(first_name, last_name, age)
VALUES ('Natalia', 'Sosiska', 20);

INSERT INTO animator(first_name, last_name, age)
VALUES ('Tamara', 'Krovopoica', 25);

INSERT INTO animator(first_name, last_name, age)
VALUES ('Tamara', 'Torbina', 21);

INSERT INTO animator(first_name, last_name, age)
VALUES ('Vadim', 'Volkodav', 19);

CREATE TABLE master_class
(
    id                     INT NOT NULL AUTO_INCREMENT,
    name                   VARCHAR(255),
    direction_id           INT NOT NULL,
    number_of_participants INT,
    PRIMARY KEY (id),
    CONSTRAINT master_class_direction_fk FOREIGN KEY (direction_id) REFERENCES direction (id) ON DELETE CASCADE
);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Veterinarian and Emergency Veterinarian', (SELECT id FROM direction WHERE name = 'Veterinarian'), 10);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Veterinary pharmacist', (SELECT id FROM direction WHERE name = 'Veterinarian'), 15);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Private police', (SELECT id FROM direction WHERE name = 'Policeman'), 5);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Police commander', (SELECT id FROM direction WHERE name = 'Policeman'), 20);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Detective', (SELECT id FROM direction WHERE name = 'Policeman'), 25);

INSERT INTO master_class(name, direction_id, number_of_participants)
VALUES ('Firefighter Rescuer', (SELECT id FROM direction WHERE name = 'Firefighter'), 15);

CREATE TABLE master_class_booking
(
    id              INT      NOT NULL AUTO_INCREMENT,
    master_class_id INT      NOT NULL,
    animator_id     INT      NOT NULL,
    start_date      DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT master_class_booking_master_class_id_fk FOREIGN KEY (master_class_id) REFERENCES master_class (id) ON DELETE CASCADE,
    CONSTRAINT master_class_booking_animator_id_fk FOREIGN KEY (animator_id) REFERENCES animator (id)
);

INSERT INTO master_class_booking(master_class_id, animator_id, start_date)
VALUES ((SELECT id FROM master_class WHERE id = '1'),
        (SELECT id FROM animator WHERE id = '1'),
        (TIMESTAMP('2021-05-23')));

INSERT INTO master_class_booking(master_class_id, animator_id, start_date)
VALUES ((SELECT id FROM master_class WHERE id = '2'),
        (SELECT id FROM animator WHERE id = '1'),
        (TIMESTAMP('2021-05-24')));

INSERT INTO master_class_booking(master_class_id, animator_id, start_date)
VALUES ((SELECT id FROM master_class WHERE id = '3'),
        (SELECT id FROM animator WHERE id = '2'),
        (TIMESTAMP('2021-05-22')));

INSERT INTO master_class_booking(master_class_id, animator_id, start_date)
VALUES ((SELECT id FROM master_class WHERE id = '4'),
        (SELECT id FROM animator WHERE id = '4'),
        (TIMESTAMP('2021-05-30')));
