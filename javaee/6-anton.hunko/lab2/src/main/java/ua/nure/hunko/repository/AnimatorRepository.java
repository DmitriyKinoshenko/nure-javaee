package ua.nure.hunko.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.nure.hunko.model.Animator;

import java.util.List;

@Repository
public interface AnimatorRepository extends JpaRepository<Animator, Integer> {

	@Query("from Animator a where 1 > (select count(b) from MasterClassBooking b where b.id = a.id and b.startDate > current_timestamp)")
	List<Animator> findAnimatorsWithoutMasterClasses();
}
