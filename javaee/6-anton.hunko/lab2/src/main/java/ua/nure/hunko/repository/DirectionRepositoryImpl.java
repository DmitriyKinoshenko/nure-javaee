package ua.nure.hunko.repository;

import org.springframework.stereotype.Repository;
import ua.nure.hunko.model.Direction;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("directionRepository")
public class DirectionRepositoryImpl implements DirectionRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Direction> findAllDirections() {
		return entityManager
				.createQuery("select d from Direction d", Direction.class)
				.getResultList();
	}

	@Override
	public Direction findDirectionById(Integer id) {
		return entityManager.find(Direction.class, id);
	}
}
