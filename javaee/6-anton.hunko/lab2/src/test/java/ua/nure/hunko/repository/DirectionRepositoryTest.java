package ua.nure.hunko.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.hunko.model.Direction;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DirectionRepositoryTest {

	private static final int ID = 9999;

	@Mock
	private TypedQuery<Direction> typedQuery;
	@Mock
	private Direction direction;
	@Mock
	private EntityManager entityManager;

	@InjectMocks
	private DirectionRepositoryImpl underTest;

	@Test
	public void shouldFindAllDirections() {
		when(entityManager.createQuery(anyString(), eq(Direction.class))).thenReturn(typedQuery);
		when(typedQuery.getResultList()).thenReturn(Collections.singletonList(direction));

		List<Direction> allDirections = underTest.findAllDirections();

		assertEquals(Collections.singletonList(direction), allDirections);
	}

	@Test
	public void shouldFindDirectionById() {
		when(entityManager.find(Direction.class, ID)).thenReturn(direction);

		Direction returnedDirection = underTest.findDirectionById(ID);

		assertEquals(direction, returnedDirection);
	}
}
