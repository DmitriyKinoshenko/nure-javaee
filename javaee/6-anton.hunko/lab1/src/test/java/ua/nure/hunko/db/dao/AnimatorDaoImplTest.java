package ua.nure.hunko.db.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.hunko.db.ConnectionHolder;
import ua.nure.hunko.db.exception.DaoException;
import ua.nure.hunko.db.extractor.Extractor;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.util.SqlQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AnimatorDaoImplTest {

	private static final int DUMMY_INT_DATA = 1;

	@Spy
	private Animator dummyAnimator;
	@Mock
	private ConnectionHolder connectionHolder;
	@Mock
	private Connection connection;
	@Mock
	private Statement statement;
	@Mock
	private PreparedStatement preparedStatement;
	@Mock
	private ResultSet resultSet;
	@Mock
	private Extractor<Animator> animatorExtractor;

	@InjectMocks
	private AnimatorDaoImpl underTest;

	@Before
	public void setUp() throws SQLException {
		dummyAnimator = new Animator();
		dummyAnimator.setId(DUMMY_INT_DATA);
		dummyAnimator.setAge(DUMMY_INT_DATA);

		when(connectionHolder.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(statement);
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
		when(statement.executeQuery(anyString())).thenReturn(resultSet);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);

		when(resultSet.next()).thenReturn(true, false);
	}

	@Test
	public void shouldFindAnimatorsWithoutMasterClasses() throws SQLException {
		when(animatorExtractor.extract(resultSet)).thenReturn(dummyAnimator);

		List<Animator> animators = underTest.findAnimatorsWithoutMasterClasses();

		assertEquals(Collections.singletonList(dummyAnimator), animators);
	}

	@Test(expected = DaoException.class)
	public void shouldThrowDaoExceptionWhenExceptionOccurredWhileFindingAnimatorsWithoutMasterClasses() throws SQLException {
		when(statement.executeQuery(anyString())).thenThrow(new SQLException());

		underTest.findAnimatorsWithoutMasterClasses();
	}

	@Test
	public void shouldAddAnimator() throws SQLException {
		when(preparedStatement.executeUpdate()).thenReturn(SqlQueries.ROW_UPDATED);

		boolean result = underTest.addAnimator(dummyAnimator);

		assertTrue(result);
	}

	@Test(expected = DaoException.class)
	public void shouldThrowDaoExceptionWhenExceptionOccurredWhileAddingAnimator() throws SQLException {
		when(preparedStatement.executeUpdate()).thenThrow(new SQLException());

		underTest.addAnimator(dummyAnimator);
	}

	@Test
	public void shouldFindAnimatorById() throws SQLException {
		when(animatorExtractor.extract(resultSet)).thenReturn(dummyAnimator);

		Animator animator = underTest.findAnimatorById(DUMMY_INT_DATA);

		assertEquals(dummyAnimator, animator);
	}

	@Test(expected = DaoException.class)
	public void shouldThrowDaoExceptionWhenExceptionOccurredWhileFindingAnimatorById() throws SQLException {
		when(preparedStatement.executeQuery()).thenThrow(new SQLException());

		underTest.findAnimatorById(DUMMY_INT_DATA);
	}
}
