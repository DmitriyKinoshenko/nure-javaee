package ua.nure.hunko.db.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.nure.hunko.db.mapper.MasterClassMapper;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;
import ua.nure.hunko.util.SqlQueries;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MasterClassDaoImplTest {

	@Spy
	private MasterClass masterClass;
	@Spy
	private Direction direction;
	@Mock
	private JdbcTemplate jdbcTemplate;
	@Mock
	private MasterClassMapper masterClassMapper;

	@InjectMocks
	private MasterClassDaoImpl underTest;

	@Test
	public void shouldFindMasterClassesByDirection() {
		when(jdbcTemplate.query(anyString(), eq(masterClassMapper), any())).thenReturn(Collections.singletonList(masterClass));

		List<MasterClass> masterClasses = underTest.findMasterClassesByDirection(direction);

		assertEquals(Collections.singletonList(masterClass), masterClasses);
	}

	@Test
	public void shouldAddMasterClass() {
		when(jdbcTemplate.update(anyString(), new Object[]{any()})).thenReturn(SqlQueries.ROW_UPDATED);

		boolean result = underTest.addMasterClass(masterClass);

		assertTrue(result);
	}
}
