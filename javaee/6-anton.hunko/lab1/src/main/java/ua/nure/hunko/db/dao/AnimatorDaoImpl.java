package ua.nure.hunko.db.dao;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.nure.hunko.db.ConnectionHolder;
import ua.nure.hunko.db.exception.DaoException;
import ua.nure.hunko.db.extractor.Extractor;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.util.SqlQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class AnimatorDaoImpl implements AnimatorDao {

	private static final Logger LOGGER = LogManager.getLogger(AnimatorDaoImpl.class);

	private final ConnectionHolder connectionHolder;
	private final Extractor<Animator> animatorExtractor;

	@Override
	public List<Animator> findAnimatorsWithoutMasterClasses() {
		List<Animator> animators = new ArrayList<>();

		Connection connection = connectionHolder.getConnection();
		try (Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(SqlQueries.ANIMATOR_FIND_ALL_WITHOUT_MASTERCLASSES)) {
			while (resultSet.next()) {
				animators.add(animatorExtractor.extract(resultSet));
			}
		} catch (SQLException e) {
			String message = "Error occurred while finding all animators";
			LOGGER.error(message, e);
			throw new DaoException(message, e);
		}

		return animators;
	}

	@Override
	public boolean addAnimator(Animator animator) {
		Connection connection = connectionHolder.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.ANIMATOR_ADD)) {
			prepareAddAnimatorStatement(statement, animator);
			return statement.executeUpdate() == 1;
		} catch (SQLException e) {
			String message = String.format("Error occurred while adding new animator with id %s", animator.getId());
			LOGGER.error(message, e);
			throw new DaoException(message, e);
		}
	}

	private void prepareAddAnimatorStatement(PreparedStatement statement, Animator animator) throws SQLException {
		int i = 1;

		statement.setInt(i++, animator.getId());
		statement.setString(i++, animator.getFirstName());
		statement.setString(i++, animator.getLastName());
		statement.setInt(i, animator.getAge());

	}

	@Override
	public Animator findAnimatorById(Integer id) {
		Connection connection = connectionHolder.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.ANIMATOR_FIND_BY_ID)) {
			statement.setInt(1, id);
			try (ResultSet resultSet = statement.executeQuery()) {
				return resultSet.next()
						? animatorExtractor.extract(resultSet)
						: null;
			}
		} catch (SQLException e) {
			String message = String.format("Error occurred while finding animator with id %s", id);
			LOGGER.error(message, e);
			throw new DaoException(message, e);
		}
	}
}
