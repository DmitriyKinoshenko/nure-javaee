package ua.nure.hunko.db.dao;

import ua.nure.hunko.model.Animator;

import java.util.List;

public interface AnimatorDao {

	List<Animator> findAnimatorsWithoutMasterClasses();

	boolean addAnimator(Animator animator);

	Animator findAnimatorById(Integer id);
}
