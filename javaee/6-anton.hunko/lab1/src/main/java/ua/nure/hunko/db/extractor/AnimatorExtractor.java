package ua.nure.hunko.db.extractor;

import ua.nure.hunko.model.Animator;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnimatorExtractor implements Extractor<Animator> {
	@Override
	public Animator extract(ResultSet resultSet) throws SQLException {
		Animator animator = new Animator();

		animator.setId(resultSet.getInt("id"));
		animator.setFirstName(resultSet.getString("first_name"));
		animator.setLastName(resultSet.getString("last_name"));
		animator.setAge(resultSet.getInt("age"));

		return animator;
	}
}
