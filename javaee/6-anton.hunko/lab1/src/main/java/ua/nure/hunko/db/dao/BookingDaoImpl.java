package ua.nure.hunko.db.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.nure.hunko.db.mapper.BookingMapper;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.Booking;
import ua.nure.hunko.util.SqlQueries;

import java.sql.Timestamp;
import java.util.List;

@RequiredArgsConstructor
public class BookingDaoImpl implements BookingDao {

	private final JdbcTemplate jdbcTemplate;
	private final BookingMapper bookingMapper;

	@Override
	public List<Booking> findBookingsByAnimator(Animator animator) {
		return jdbcTemplate.query(SqlQueries.BOOKING_FIND_BY_ANIMATOR, bookingMapper, animator.getId());
	}

	@Override
	public List<Booking> findBookingsBetweenDates(Timestamp from, Timestamp to) {
		return jdbcTemplate.query(SqlQueries.BOOKING_FIND_BETWEEN_DATES, bookingMapper, from, to);
	}

	@Override
	public List<Booking> findBookingsFrom(Timestamp from) {
		return jdbcTemplate.query(SqlQueries.BOOKING_FIND_FROM, bookingMapper, from);
	}

	@Override
	public boolean updateDateOfMasterClass(Booking booking, Timestamp newDate) {
		return SqlQueries.ROW_UPDATED == jdbcTemplate.update(
				SqlQueries.BOOKING_UPDATE_START_DATE,
				newDate,
				(booking.getMasterClass()) != null
						? booking.getMasterClass().getId()
						: null
		);
	}
}
