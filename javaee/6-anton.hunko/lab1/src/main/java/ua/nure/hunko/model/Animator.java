package ua.nure.hunko.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Animator {

	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
}
