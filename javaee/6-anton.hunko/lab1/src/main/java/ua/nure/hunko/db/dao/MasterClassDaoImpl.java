package ua.nure.hunko.db.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.nure.hunko.db.mapper.MasterClassMapper;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;
import ua.nure.hunko.util.SqlQueries;

import java.util.List;

@RequiredArgsConstructor
public class MasterClassDaoImpl implements MasterClassDao {

	private final JdbcTemplate jdbcTemplate;
	private final MasterClassMapper masterClassMapper;

	@Override
	public List<MasterClass> findMasterClassesByDirection(Direction direction) {
		return jdbcTemplate.query(
				SqlQueries.MASTER_CLASS_FIND_BY_DIRECTION,
				masterClassMapper,
				direction.getId());
	}

	@Override
	public boolean addMasterClass(MasterClass masterClass) {
		return SqlQueries.ROW_UPDATED == jdbcTemplate.update(
				SqlQueries.MASTER_CLASS_ADD,
				masterClass.getName(),
				masterClass.getNumberOfParticipants(),
				(masterClass.getDirection() != null)
						? masterClass.getDirection().getId()
						: null
		);
	}
}
