package ua.nure.hunko.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Direction {

	private int id;
	private String name;
}
