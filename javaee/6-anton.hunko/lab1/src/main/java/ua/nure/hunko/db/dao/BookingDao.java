package ua.nure.hunko.db.dao;

import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.Booking;

import java.sql.Timestamp;
import java.util.List;

public interface BookingDao {

	List<Booking> findBookingsByAnimator(Animator animator);

	List<Booking> findBookingsBetweenDates(Timestamp from, Timestamp to);

	List<Booking> findBookingsFrom(Timestamp from);

	boolean updateDateOfMasterClass(Booking booking, Timestamp newDate);

}
