package ua.nure.hunko.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class Booking {

	private MasterClass masterClass;
	private List<Animator> animators;
	private Timestamp dateOfStart;
}
