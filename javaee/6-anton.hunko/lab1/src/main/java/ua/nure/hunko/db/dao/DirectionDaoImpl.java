package ua.nure.hunko.db.dao;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.nure.hunko.db.ConnectionHolder;
import ua.nure.hunko.db.exception.DaoException;
import ua.nure.hunko.db.extractor.Extractor;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.util.SqlQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class DirectionDaoImpl implements DirectionDao {

	private static final Logger LOGGER = LogManager.getLogger(DirectionDaoImpl.class);

	private final ConnectionHolder connectionHolder;
	private final Extractor<Direction> directionExtractor;

	@Override
	public List<Direction> findAllDirections() {
		List<Direction> directions = new ArrayList<>();

		Connection connection = connectionHolder.getConnection();
		try (Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(SqlQueries.DIRECTION_FIND_ALL)) {
			while (resultSet.next()) {
				directions.add(directionExtractor.extract(resultSet));
			}
		} catch (SQLException e) {
			String message = "Error occurred while finding all directions";
			LOGGER.error(message, e);
			throw new DaoException(message, e);
		}

		return directions;
	}

	@Override
	public Direction findDirectionById(Integer id) {
		Connection connection = connectionHolder.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(SqlQueries.DIRECTION_FIND_BY_ID)) {
			statement.setInt(1, id);
			try (ResultSet resultSet = statement.executeQuery()) {
				return resultSet.next()
						? directionExtractor.extract(resultSet)
						: null;
			}
		} catch (SQLException e) {
			String message = String.format("Error occurred while finding direction with id %s", id);
			LOGGER.error(message, e);
			throw new DaoException(message, e);
		}
	}
}
