package ua.nure.hunko.db.transaction;

@FunctionalInterface
public interface Operation<T, E extends Throwable> {
	T execute() throws E;
}
