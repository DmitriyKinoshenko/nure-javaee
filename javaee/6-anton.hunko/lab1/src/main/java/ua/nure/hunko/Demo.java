package ua.nure.hunko;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.nure.hunko.config.DatabaseConfiguration;
import ua.nure.hunko.db.ConnectionHolder;
import ua.nure.hunko.db.dao.AnimatorDao;
import ua.nure.hunko.db.dao.AnimatorDaoImpl;
import ua.nure.hunko.db.dao.DirectionDao;
import ua.nure.hunko.db.dao.DirectionDaoImpl;
import ua.nure.hunko.db.dao.MasterClassDao;
import ua.nure.hunko.db.dao.MasterClassDaoImpl;
import ua.nure.hunko.db.extractor.AnimatorExtractor;
import ua.nure.hunko.db.extractor.DirectionExtractor;
import ua.nure.hunko.db.extractor.Extractor;
import ua.nure.hunko.db.mapper.MasterClassMapper;
import ua.nure.hunko.db.transaction.TransactionManager;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;

import java.sql.SQLException;
import java.util.List;

public class Demo {

	private static final Logger LOGGER = LogManager.getLogger(Demo.class);

	public static void main(String[] args) throws SQLException, ConfigurationException {
		DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration("application.properties");

		Extractor<Animator> animatorExtractor = new AnimatorExtractor();
		ConnectionHolder connectionHolder = new ConnectionHolder();

		TransactionManager transactionManager = new TransactionManager(connectionHolder, databaseConfiguration.getDataSource());
		AnimatorDao animatorDao = new AnimatorDaoImpl(connectionHolder, animatorExtractor);

		List<Animator> animators = transactionManager.execute(animatorDao::findAnimatorsWithoutMasterClasses);
		animators.forEach((animator) -> {
			LOGGER.info(String.format("Animator: { %s, %s, %s, %s }",
					animator.getId(),
					animator.getFirstName(),
					animator.getLastName(),
					animator.getAge()));
		});

		JdbcTemplate jdbcTemplate = new JdbcTemplate(databaseConfiguration.getDataSource());
		MasterClassMapper masterClassMapper = new MasterClassMapper();

		DirectionExtractor directionExtractor = new DirectionExtractor();

		DirectionDao directionDao = new DirectionDaoImpl(connectionHolder, directionExtractor);
		Direction direction = transactionManager.execute(() -> directionDao.findDirectionById(1));

		MasterClassDao masterClassDao = new MasterClassDaoImpl(jdbcTemplate, masterClassMapper);

		MasterClass masterClass = new MasterClass();
		masterClass.setName("Demo");
		masterClass.setDirection(direction);
		masterClass.setNumberOfParticipants(5);

		LOGGER.info(String.format("Result of adding: %s", masterClassDao.addMasterClass(masterClass)));
	}
}
