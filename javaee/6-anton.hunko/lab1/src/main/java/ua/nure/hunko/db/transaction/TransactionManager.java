package ua.nure.hunko.db.transaction;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.nure.hunko.db.ConnectionHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@RequiredArgsConstructor
public class TransactionManager {

	private static final Logger LOGGER = LogManager.getLogger(TransactionManager.class);

	private final ConnectionHolder connectionHolder;
	private final DataSource dataSource;

	public <T, E extends Throwable> T execute(Operation<T, E> operation) throws SQLException, E {
		Connection connection = null;
		T result;
		try {
			LOGGER.debug("Obtaining connection from data source");
			connection = dataSource.getConnection();
			connectionHolder.setConnection(connection);
			LOGGER.debug("Executing operation");
			result = operation.execute();
			LOGGER.debug("Committing transaction");
			commit(connection);
		} catch (Throwable e) {
			LOGGER.error(String.format("Error occurred: %s", e.getMessage()));
			LOGGER.error("Rollback transaction");
			rollback(connection);
			throw (E) e;
		} finally {
			connectionHolder.removeConnection();
			LOGGER.debug("Closing connection");
			close(connection);
		}
		return result;
	}

	private void commit(Connection connection) throws SQLException {
		if (connection != null) {
			connection.commit();
		}
	}

	private void rollback(Connection connection) throws SQLException {
		if (connection != null) {
			connection.rollback();
		}
	}

	private void close(Connection connection) throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}
}
