# Laboratory work №1

-`author`: Anton Hunko

## How to use

1. Go to `lab1/src/main/docker`
2. Run the following script: `docker-compose -f maria-db.yml up`
3. Run `Demo.main()` method

# Описание предметной области:

### Расписание мастер-классов в развлекательном центре

1. `direction(id, name)`
2. `animator(id, first_name, last_name, age)`
3. `master_class(id, name, direction_id, number_of_participants)`
4. `master_class_booking(master_class_id, animator_id, start_date)`

### Необходимо выполнить следующие запросы:

1. Поиск аниматоров, у которых нет мастер-классов
2. Поиск мастер-классов по направлению
3. Поиск записей мастер-классов по аниматорам
4. Поиск записей мастер-классов по дате

### Необходимо выполнить следующие вставки и изменения:

1. Добавить нового аниматора
2. Изменить дату проведения мастер-класса
3. Добавить мастер-класс
