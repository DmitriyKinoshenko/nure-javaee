package ua.kture.tpr;

import static ua.kture.tpr.Bidder.cheaterBidder;
import static ua.kture.tpr.Bidder.honestBidder;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
public class Auction {

  public static void main(String[] args) {

    /*
    На торги выставляется несколько лотов. Участники аукциона делают заявки.
     Заявку можно корректировать в сторону увеличения несколько раз за торги одного лота.
      Аукцион определяет победителя и переходит к следующему лоту.
      Участник, не заплативший за лот в заданный промежуток времени,
      отстраняется на несколько лотов от торгов.
     */

//    testSingleBidder();

//    testTwoBidders();

    testCheatBidders();
  }

  private static void testSingleBidder() {
    var lot = new Lot("Ancient greek gold coin", 30);
    var auctionDuration = new Random().nextInt(40000) + 10000;
    log.info("Auction duration: {}, bidding for {}", auctionDuration, lot);

    var bidder = honestBidder("John");
    var bidPublisher = bidder.bidForLot(lot);

    var winnerBid = bidPublisher
        .doOnNext(bid -> log.info("Received next bid: {}", bid))
        .take(Duration.ofMillis(auctionDuration))
        .doOnComplete(() -> log.info("Auction is finished"))
        .blockLast();

    log.info("The winner bid is {}", winnerBid);
  }

  private static void testTwoBidders() {
    var lot = new Lot("Ancient greek gold coin", 30);
    var auctionDuration = new Random().nextInt(40000) + 10000;
    log.info("Auction duration: {}, bidding for {}", auctionDuration, lot);

    var bidders = List.of(honestBidder("John"), honestBidder("Mary"));

    var bidPublishers = bidders.stream()
        .map(bidder -> bidder.bidForLot(lot))
        .collect(Collectors.toList());

    var winnerBid = Flux.merge(bidPublishers)
        .doOnNext(bid -> log.info("Received next bid: {}", bid))
        .take(Duration.ofMillis(auctionDuration))
        .doOnComplete(() -> log.info("Auction is finished"))
        .blockLast();

    log.info("The winner bid is {}", winnerBid);
  }

  private static void testCheatBidders() {
    var lot = new Lot("Ancient greek gold coin", 30);
    var auctionDuration = new Random().nextInt(40000) + 10000;
    log.info("Auction duration: {}, bidding for {}", auctionDuration, lot);

    var bidders = List.of(cheaterBidder("John"), honestBidder("Mary"), cheaterBidder("Anna"));

    var bidPublishers = bidders.stream()
        .map(bidder -> bidder.bidForLot(lot))
        .collect(Collectors.toList());

    var winnerBid = Flux.merge(bidPublishers)
        .doOnNext(bid -> log.info("Received next bid: {}", bid))
        .take(Duration.ofMillis(auctionDuration))
        .doOnComplete(() -> log.info("Auction is finished"))
        .blockLast();


    if (winnerBid != null) {
      try {
        takeMoney(winnerBid);
        log.info("The winner bid is {}", winnerBid);
      } catch (BidCheatException e) {
        log.warn("Bid of {} is revoked due to a cheat of {}", lot, winnerBid.getBidder());
      }

    } else {
      log.warn("Lot {} has not been traded", lot);
    }

  }

  private static void takeMoney(Bid bid) {
    var bidder = bid.getBidder();

    if (!bidder.payForLot(bid.getLot())) {
      bidder.ban(getBanTurns());
      throw new BidCheatException(String.format("%s cheated!", bidder));
    }
  }

  private static int getBanTurns() {
    return new Random().nextInt(3) + 1;
  }

}
