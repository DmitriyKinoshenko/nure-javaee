package ua.kture.tpr;

import lombok.ToString;

@ToString
public class Lot {

  private String name;
  private int initialPrice;
  private int currentPrice;

  public Lot(String name, int initialPrice) {
    this.name = name;
    this.initialPrice = initialPrice;
    currentPrice = initialPrice;
  }

  public synchronized int getCurrentPrice() {
    return currentPrice;
  }

  public synchronized void nextBid(int newPrice) {
    currentPrice = newPrice;
  }
}
