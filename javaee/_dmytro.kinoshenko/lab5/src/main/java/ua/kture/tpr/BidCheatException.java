package ua.kture.tpr;

public class BidCheatException extends RuntimeException {

  public BidCheatException(String message) {
    super(message);
  }
}
